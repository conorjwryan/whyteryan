# CHANGELOG

---

Below shows the completed issues in the `conorjwryan-site` repository

---

## 2024-08-06

- #92 - (Updated Site Design Structure)

## 2024-08-03

- #75 - (Added Pagination to Posts List)
- #86 - (Updated box shortcode to fix bug with relative links)
- #91 - (Updated Footer with gitInfo Information for Versioning)

## 2024-08-01

- #84 - (Added New Services Section to Site)
- #88 - (Added Email Service Page)
- #87 - (Updated Project Overview and Design Structure)

## 2024-07-29

- #85 - (Updated CSS, Article Span and Title Info for Hugo Upgrade 0.117.0 to 0.129.0)

## 2024-01-23

- #58 - (Added Ara Setup Tutorial)

## 2023-08-29

- #79 - (Added Signature Shortcode)
- #78 - (Update post footer CSS)

## 2023-08-28

- #77 - (Updated Navigation Logo)
- #76 - (Fixed Front Page Profile Styling)

## 2023-08-25

- #21 - (Added About Page)

## 2023-08-24

- #74 - (Updated Favicons)
- #70 #71 #72 #73 - (Tasks Part of #69 - DELETED)
- #69 - (Changed Theme from Gokarna to PaperMod)

## 2023-08-17

- #65 - (Added Update Notification)
- #60 - (Added TL;DR / Skip Tutorial Boxes to Posts)
- #62 - (Added Info Boxes to Posts)
- #68 - (Fixed Bug with Tag Text Overflow)

## 2023-08-16

- #66 - (Updated Project Overview for Versioning and Improved Logic and Design Structure)

## 2023-08-15

- #56 - (Redesigned Index Page)
- #44 - (Added Tags to Post List and Main Page)

## 2023-08-14

- #61 - (Added AKA for Projects)
- #63 - (Hid Additional Tags on Hero Cards)
- #59 - (Fixed Bug: Project Title DIV not Being Responsive)
- #64 - (Closed: GitInfo not working on Cloudflare Pages)

## 2023-08-13

- #57 - (Added Cloudflare Tunnel Reverse Proxy Post)

## 2023-08-08

- #38 - (Archived - Post About Changes to site)

## 2023-08-04

- #55 - (Fixed Bug with Draft Script)

## 2023-08-03

- #45 - (Cloudflare R2 CDN Hugo Post)

## 2023-08-02

- #53 - (Changed Project Front Matter)
- #50 - (Added Bash Scripts Project)
- #54 - (Added Ansible Project)
- #52 - (Added Ara Project)

## 2023-08-01

- #51 - (Improved Markdown and Shell Script Through Linter)

## 2023-01-26

- #49 - (Updating Favicon)

## 2022-11-01

- #17 - (Hugo Cloudflare Pages Considerations Post)

## 2022-10-30

- #47 - (Add Website Monitoring Code)

## 2022-10-29

- #46 - (Redirect Fixed)

## 2022-10-28

- #37 - (Refactor Featured Image Partials)

## 2022-10-27

- #37 - (Refactor IMG Shortcode)

## 2022-10-26

- #43 - (Refine Draft Upload Script)

## 2022-10-23

- #31 - (Draft Post Upload Script)

## 2022-10-20

- #42 - (Cloudflare Deployment URL Redirected)
- #40 - (Fixed Nav Links)
- #41 - (Tags Not Linking on Posts)

## 2022-10-19

- #34 - (GitInfo Cloudflare Hugo Post)
- #26 - REOPENED (Cloudflare GitInfo Testing as Per #34)

## 2022-10-18

- #39 - (Fixed Project Information Status Bug)

## 2022-19-14

- #36 - (Format Tag Term Page)

## 2022-10-13

- #35 - (Fixed Mobile CSS)

## 2022-10-12

- #29 - (Change How Images Viewed on Blog Cards)

## 2022-10-11

- #26 - REOPENED (Cloudflare GitInfo Test)
- #33 - (Fix Cloudflare Deployment URLs)

## 2022-10-10

- #30 - (Updated Default Post Archetype)
- #32 - (Fix Image Alt Properties and Featured Image Caption)
- #18 - (Personal Letterboxd Project Into Post)

## 2022-10-09

- #13 - (CMS Organisation)
- #28 - (Change Blog Post Cards)

## 2022-10-07

- #27 - (Fix Whitespace CSS in Project Terms List)
- #26 - (GitInfo in Post Footer)

## 2022-10-05

- #23 - (Project Taxonomy Cleanup)

## 2022-10-04

- #25 - (Blog Cards Implementation)

## 2022-10-03

- #24 - (Personal Website Project Page)
- #22 - (Movie Madness Project Page)
- #20 - (Post Positioning)
- #19 - (Fix CF Forwarding)
- #9 - (Project Taxonomy)

## 2022-09-27

- #15 - (Switched to New Domain)
- #16 - (Update Pages to include tags)

## 2022-09-26

- #14 - (Refactor Image Conventions)

## 2022-09-25

- #11 - (Initial Post Hugo, Wordpress, Complacency)
- #12 - (Img Border)

## 2022-09-23

- #8 - (Create Image Shortcode)

## 2022-09-20

- #7 - (Explore CDN Options)

## 2022-09-18

- #6 - (Fix Favicons)
- #5 - (Produce Featured Image on Page and Posts)

## 2022-09-17

- #4 - (General Cleanup of Website)

## 2022-09-15

- #3 - (Fix Duplicate Theme)

## 2022-09-14

- #2 - (Deploy Hugo Site on Cloudflare Pages)
- #1 - (Set up Zapier Gitlab to Todoist)
