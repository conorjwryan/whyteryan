# Hello

This is the repo for the site: https://www.conorjwryan.com

This is my personal blog based on [Hugo](https://gohugo.io), with a customised ["Papermod" theme](https://github.com/adityatelange/hugo-PaperMod), hosted on [Cloudflare Pages](https://pages.cloudflare.com/) which houses tutorials on `Hugo`, `Cloudflare` and other security-based topics as well as a base for my open-source project work. See more of my repositories [here](https://gitlab.com/conorjwryan) 

Look at the [CHANGELOG](https://gitlab.com/conorjwryan/conorjwryan-site/-/blob/main/CHANGELOG.md) to see the improvements I've made to the site.