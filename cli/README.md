# Scripts

Welcome! Below I have included the scripts used in this project to help automate the process of creating / maintaining posts in Hugo.

## Draft Workflow Script

A script for uploading images after the draft is completed

### Overview

The `Complete Draft` Script is a bash script which helps automate (somewhat) end process of putting a draft and associated images in different places for production.

When I have been working with draft posts I had a problem where I had to...

1. Rename all images to "draft-name"-"image-name".jpg
2. Upload them to the CDN
3. Change post frontmatter from `draft: true` to `draft: false`
4. Move/Rename `index.md` to posts folder
5. Delete all empty folders

Doing this all manually was a long-winded mess. This script automates that process with variable paths (not hardcoded) so that if my directories change then that only requires updating the `.env.drafts` file not the script itself.  

### Script Notes

* Works on MacOS 12.6 (maybe Linux - not currently on Windows)
* To upload images this requires `rclone` to be installed and a 'Remote' configured
* Assumes draft structure is as follows:

```bash
    drafts/ 
        -> draft1
            -> images
                -> featured-image.jpg (feat.jpg) 
                -> extra-post-image.jpg
            -> index.md 
        -> draft2
        ... 
```

* Can handle any image type (.jpg)
* If no featured image (feat.jpg) then continues as normal
* If no `images` directory script exits after .md name change
* See here: [for my script creation process](https://gitlab.com/conorjwryan/conorjwryan-site/-/issues/31)

### How To Run Script

1. Download the `cli` directory to your project
2. Copy over the `example.drafts.env` file to your home directory
    `cp example.drafts.env ~/.drafts.env`
3. Make changes to the newly created `.drafts.env` file in your home directory using your preferred text editor
    `nano ~/.drafts.env`
    (extra instructions in the `.env` file)
4. Make the script executable `chmod +x complete-draft.sh`
5. Run script (assuming script in current directory) `./complete-draft.sh`

### Improvements

* One of the original points of this was to have some image compression baked in so in a future iteration would love to incorporate something like Short Pixel to compress chosen images without loss in quality to save bandwidth and page load times.

* Will do a tutorial for each of the steps in this series
* Using Cloudflare R2 As A Personal CDN
* Post Draft Archetypes In Hugo
* Automating The Drafting End Process (this script in detail)
