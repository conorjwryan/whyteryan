#!/bin/bash
# shellcheck disable=SC1091,SC2154,SC2012


if [[ ! -f "$HOME/.drafts.env" ]]; then


    echo -e "\n.drafts.env is not detected!\n";
    echo -e "Please ensure you have copied and changed the example file\n";
    echo -e "Script now Exiting";
    exit 1;

fi

# Load env variables
set -o allexport
source "$HOME/.drafts.env"
set +o allexport

clear;

echo "--------------------------------";
echo "Move Drafts to Production Script";
echo -e "--------------------------------\n";

# Show Script Working Directories
echo -e "The Live Posts Directory is: $POSTS_DIR\n";
echo -e "The Draft Posts Directory is $DRAFTS_DIR\n";

echo -e "The script will be working in these directories.\n";
echo -e 'If they are not please change the values in the .env file "~/.drafts.env"';

# Yes/No Option
while true; do
    read -r -p "Do you want to proceed? (y/n)" yn

    case $yn in 
        [yY]* ) echo "Okay script will proceed"; break;;
        [nN]* ) echo "Exiting..."; exit 0;;
        * ) echo "Please answer yes or no";;
esac
done

echo -e "\nMoving to drafts location\n";

cd "$DRAFTS_DIR" || exit;

pwd;

# List number of folders (posts) in drafts directory
DRAFT_NUMBER=$(ls | wc -l | sed 's/ //g')

# Validation of if more than 0 
if [[ $DRAFT_NUMBER == 0 ]]; then 

    echo -e "No folders found in draft location! Exiting now..."
    exit 1; 

fi 

echo -e "\nPlease Select Folder To Copy"

# Numbered selection to choose draft folder
select drafts_folder in *; do test -n "$drafts_folder" && break; echo ">>> Invalid Selection"; done

# Enter into Selected Folder
cd "$drafts_folder" || exit;

echo -e "\nYou have chosen $drafts_folder";
pwd

# Rename drafts_folder variable to post_name

post_name=$drafts_folder

echo -e "\nThis script will now work on the selected folder";

# Validation of if index.md is present
if [ ! -f "index.md" ]; 
then
    echo "No articles found!";
    exit 0;
fi 

# Change Hugo FrontMatter Draft Status to True
sed -i "" "s/draft: true/draft: false/g" index.md

# Move index from drafts to main posts directory
mv "index.md" "${POSTS_DIR}${post_name}.md" 

echo -e "\nHave moved the index.md to the main 'posts' directory under the name:\n${post_name}.md\n"; 

echo -e "Will now check if 'images' folder is present\n";

# Checking if image folder DOES NOT exist
if [ ! -d "images" ];
then
    echo -e "There is no 'images' folder present\n\nThe Script will now remove the ${post_name} folder and exit";
    cd ../;
    rmdir "${post_name}";
    exit 0;
fi

# If image folder does exist then carry on script 
cd "images" || exit;

# Changing the name of images to have post name prefix
for image_name in * ;
do 
    echo "Changing name from ${image_name} to ${post_name}-${image_name}";
    mv "${image_name}" "${post_name}-${image_name}";
done;

echo -e "\nHave renamed images to ${post_name}-(image-name)\n";

echo -e "Checking if featured image exists\n";

# Checking if featured Image Exists
# It might not exist due to url being put in frontmatter

feat_image_file=$(find . -name "*-feat.*" -type f -print -quit)
if [ -n "${feat_image_file}" ]; then
    echo -e "Featured Image Found exists\n"
    extension="${feat_image_file##*.}"
    mv "${feat_image_file}" "${post_name}.${extension}"

else
    echo -e "Featured Image Does Not Exist\nThis may be because the post links to an external image\n"
fi

# List New Image Names
ls

# Remove Placeholder .gitkeep File
rm .gitkeep

# Remove .DS_Store (Mac hidden finder file)

if [ -f ".DS_Store" ]; then
    echo -e "Removing .DS_Store\n";
    rm .DS_Store
fi

echo -e "\nWill now upload these to CDN\n";

cd ../

# Uploading to CDN via RCLONE
rclone move images/ "${RCLONE_CONF}${RCLONE_DIR}"

echo -e "Upload Completed. Please Check the above output for any errors.\n";

# Removing Empty Folders 
# If not empty something has gone wrong with the script

echo -e "Removing the now empty image Directory";
rmdir images

# Removing .DS_Store (Mac hidden finder file)
if [ -f ".DS_Store" ]; then
    echo -e "Removing .DS_Store\n";
    rm .DS_Store
fi

echo "";
echo "Removing ${post_name} from Drafts Folder";

cd ../
rmdir "${post_name}"

echo -e "\nScript now complete!";
exit; 
