---
title: "Example 1"
date: 2020-01-01
draft: true
type: "casestudy"
services: [email]
casestudy_tags: [example]
description: "This is an example of a case study."
featuredImage: "https://via.placeholder.com/150"
featuredImageAlt: "Placeholder image"
---

This is an example of a case study.  Let's see what happens when I add some text here. This is for testing purposes only.
