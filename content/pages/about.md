---
title: "About"
lastmod: "2023-08-25"
author: "Conor Ryan"
type: "page"
gitIssue: 21
featuredImageAlt: "Conor Ryan"
---

I am a freelance IT consultant who specialises in IT security for small businesses. I have a BSc in Geography and a Masters in Geopolitics and Security. Since the age of 10 I have been interested in computers and technology. Over the years I have worked as a broadcast engineer, a web developer, a support manager and a systems administrator. I have a passion finding out how things work and teaching others what I've learned. I use this website for tutorials and discussions on computer and internet security topics as well as a base for my `open source` projects (see more info about my projects [here](/projects/)).

When not working I enjoy watching movies (see my letterboxd) and drinking coffee.

## Social Links

[LinkedIn](https://linkedin.com/in/conorjwryan)

[Gitlab](https://gitlab.com/conorjwryan)

[Letterboxd](https://letterboxd.com/conorjwryan)

[X](https://twitter.com/conorjwryan)

## Contact

If you would like to get in contact with me about my services or if you have a question about any of my posts please [email me here](mailto:hello@conorjwryan.com)
