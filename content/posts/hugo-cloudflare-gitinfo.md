---
title: "Using Hugo's GitInfo with Cloudflare Pages"
date: 2022-10-19T18:27:44+01:00
lastmod: 2023-08-07
description: "Before Hosting Hugo on Cloudflare Pages For Versioned Content Consider This..."
type: "post"
tags: [cloudflare pages, hugo, bug]
projects: [conorjwryan]
gitIssue: 34
featuredImageAlt: "A screenshot of a post on this website showing GitInfo variable data to inform visitors when the post was last updated"
featuredImageCaption: "how gitinfo is supposed to work. with cloudflare pages it is not that easy"
---

## Update: 2024-08-07

I have gotten `.GitInfo` to work on `Cloudflare Pages` following the advice of [this blog post](https://www.brycewray.com/posts/2023/05/cloudflare-pages-more-attractive-home-hugo-sites/).

It seems with the release of the `Cloudflare Pages` `Build Engine V2` released last year it was possible to get `.GitInfo` to work although it was not as simple as changing over to `V2` as I tried previously. It is due to the way in which `Cloudflare Pages` only does a shallow clone of the repository which does not include the `.git` folder. This means that `.GitInfo` is not available to Hugo when it is building the site.

{{< img int cloudflare-pages-build-command.jpg "a screenshot of the build command in the cloudflare pages settings" "go to settings -> 'build & deployments' -> 'edit configurations' and change the build command to 'git fetch --unshallow & hugo' and ensure that the 'build system version' is 'version 2'" >}}

In order to get `.GitInfo` working I had to modify the build command in the `Cloudflare Pages` settings:

1. Navigate to the `Settings` section of your `Cloudflare Pages` project
2. Click on `Build & Deployments`
3. Click on `Edit Configurations`

    In the `Build Command` field change the command to:

    ```bash
    git fetch --unshallow && hugo
    ```

4. Ensure that the `Build System Version` is set to `Version 2`.

5. With this you should then go to `hugo config file` and paste the following code:

    ```yaml
    enableGitInfo: true
    ```

6. Paste the following code at the bottom of your `post-footer.html` partial:

    ```go-html-template
    <div class="post-footer">
        <small>
            {{ if .GitInfo }}
                {{ with .GitInfo.AuthorDate }}
                    This page was last updated: 
                    {{ dateFormat "January 2, 2006" . }}
                {{ end }}
                <br />
                {{ with .GitInfo.Subject }}
                    "{{ . }}"
                {{ end }}
                <br />
                See the history of this page by clicking here: <a
                href="https://gitlab.com/conorjwryan/conorjwryan-site/-/issues/
                {{ .Params.gitIssue }}">#{{
                .Params.gitIssue }}</a>
            {{ else }}
                This page has not been committed yet and
                thus will not show any "gitInfo"
            {{ end }}
        </small>
    </div>
    ```

7. Commit and push your changes to your repository and watch as `.GitInfo` works as intended on `Cloudflare Pages`

{{< img int cloudflare-pages-gitinfo-working.jpg "a screenshot of the gitinfo working on cloudflare pages" "gitinfo working on cloudflare pages" >}}

Success!

---

## Post Context

Over the past month I have been using `Cloudflare Pages` to [host my hugo-based blog and portfolio](https://www.conorjwryan.com/projects/conorjwryan/). Since the beginning I have seen this site as a development diary and portfolio. This site by its very nature is open-source. I want people not only to see the code but also see my developmental process and problem-solving skills. This might be useful for perspective employers but also those who love to code or those exploring using a similar development stack for client projects.

This site is set up so that Hugo and Cloudflare interact through Gitlab which acts as the [code repository for this site](https://gitlab.com/conorjwryan/conorjwryan-site). When I push commits (specific changes in code) I associate that code with a `git issue` labeled for bugs, new features, posts etc. which allows easy organisation.

{{< img int git-issues.jpg "a screen shot of the issues associated with the website" "a screenshot of 'issues' in this sites git repository" >}}

## Using Hugo's ".GitInfo" for Accountability  

[A week ago I realised](https://gitlab.com/conorjwryan/conorjwryan-site/-/issues/26) that in order to be fully in-step with my goal to make this site fully transparent I needed to create `git issues` for each of the posts and pages of this site and not just the code changes. That would allow the posts (like this one right now) to have a traceable link for people to see page changes and updates to posts when I correct spelling mistakes or update posts when new information becomes available.

This common on many news sites and in API documentation. News articles, tutorials, and APIs are not static so neither should their documentation be. As new information or features become available there should be an indication of when the change took place and why.

Thankfully for me Hugo has this functionality built in! Hugo can access the commit data of repositories hosting a static site's files through the use of [`.GitInfo` variable](https://gohugo.io/variables/git/) which can be used to tell a user when a specific page was last updated, producing something like this:

{{< img int site-last-updated.jpg "a screenshot of of 'page last updated' on the hugo documentation site showing `when and why`" "a screenshot of of 'page last updated' on the hugo documentation site showing `when and why`" "https://gohugo.io/getting-started/configuration-markup">}}

## Constructing the Code

Following the documentation on the Hugo website I needed to enable `.GitInfo` in my
 [site config file](https://gitlab.com/conorjwryan/conorjwryan-site/blob/ac6b9fada1de1706d748d2365ff64abecca5cdf3/config.toml#L8) `config.toml` by adding:

 ```toml
enableGitInfo = true
 ```

I then added the following code to a template partial called `post-footer.html`. I then called `post-footer.html` in my [`post.html` partial](https://gitlab.com/conorjwryan/conorjwryan-site/blob/ac6b9fada1de1706d748d2365ff64abecca5cdf3/layouts/partials/post.html#L25) so that `.GitInfo` can be placed at the bottom of each post/page.

```go-html-template
<div class="post-footer">
    <small>
        {{ if .GitInfo }}
            {{ with .GitInfo.AuthorDate }}
                This page was last updated: 
                {{ dateFormat "January 2, 2006" . }}
            {{ end }}
            <br />
             {{ with .GitInfo.Subject }}
                "{{ . }}"
            {{ end }}
            <br />
            See the history of this page by clicking here: <a
            href="https://gitlab.com/conorjwryan/conorjwryan-site/-/issues/
            {{ .Params.gitIssue }}">#{{
            .Params.gitIssue }}</a>
        {{ else }}
            This page has not been committed yet and
             thus will not show any "gitInfo"
        {{ end }}
    </small>
</div>
```

The above code is placed at the bottom of each post. It first checks if `.GitInfo` is available.

**If conditions are met:**  it proceeds to add `This page was last updated October 10, 2022` (for example). Below that shows the message from the last git commit `"#32 added feat img caption and link for post imgs"`. Below that it invites the user to see the accompanying git issue (only available if you [add `gitissue` parameter to your post frontmatter](https://gitlab.com/conorjwryan/conorjwryan-site/blob/ac6b9fada1de1706d748d2365ff64abecca5cdf3/content/posts/the-personal-letterboxd-project.md#L9)).

{{< img int git-info-local.jpg "the result of the above code stating the use of gitinfo at the end of each page" "showing the result of the gitinfo at the bottom of a post in local development mode" >}}

**If conditions are NOT met:** (such as when a page has not been committed to the repository - like this post in draft form) then below it states `This page has not been committed yet and thus will not show any "gitInfo"`

{{< img int no-git-info-local.jpg "A Screenshot of this page under construction not and thus having not been committed informing the author of such by not meeting the conditions of the code above" "Notice of this post not being committed during post draft stage" >}}

### Potential Errors in Local Development

The `if/else` statement checking for the existence of `GitInfo` is the most important part of the [above code](#constructing-the-code). If you call any `.GitInfo` term without it like `.AuthorDate` then hugo will throw out an error saying `term is null` like the error below:

{{< img int local-error.jpg "An error error stating 'term is nil' when trying to work out how to incorporate 'GitInfo' in local development">}}

Errors like this are so overwhelming it took me ages to realise what the cause was and that all I required to fix it was the `{{ if .GitInfo }}`. I thought `hugo serve` was broken as the code would work while in active development but as soon as I started `hugo serve` fresh it would produce errors. I figured out a "hotfix" [where it would only show `.GitInfo` "in production"](https://gitlab.com/conorjwryan/conorjwryan-site/-/commit/e839255a918535ea0635240d4b095ebb9e371e0c). This post would've been a different story entirely if I didn't realise the mistake was purely mine.

## .GitInfo In Production on Cloudflare Pages

The result from the above code is that each and every page and post has different `GitInfo` stating when the page was last updated. Below is a selection of three posts on this site showing how `GitInfo is supposed to work (shown in local development environment).

{{< img int git-info-correct-crop.jpg "a composite screenshot of gitinfo on 3 different posts/pages. Each have different commit messages" "how gitinfo works in  the development environment 'hugo serve'">}}

On my production environment deployed on `Cloudflare Pages` it is a different story entirely. All posts/pages default to the last commit message/date in the repo. **This defeats the whole purpose of including `.GitInfo` in the first place!**

{{< img int git-info-on-cloudflare.jpg "a composite screenshot of gitinfo on deployed Cloudflare Pages which breaks the gitinfo variable" "the same posts as above hosted on 'cloudflare pages' which replaces all individual gitinfo-based information with the latest commit message/date" >}}

Having doubted myself thinking that this was a problem with my code rather then the platform itself I deployed the same instance on `Netlify` (another service offering free hosting provider/deployment engine for Jamstac applications/static sites) and it had no problem displaying the correct `.GitInfo` information. This leads me to believe that this is not a `Hugo` issue but rather a `Cloudflare Pages`-specific platform issue resulting from the way it clones/caches repositories and deals with specific git commit refs.

## Decisions, Decisions

I have no doubt this will be fixed in time but there is no telling when this will happen. However, when it does I will be sure to update this page. In the meantime there are two options I am presented with if I want to be able to use `.GitInfo`:

1. Choose another hosting provider like Netlify that can properly show `.GitInfo`

2. Wait for Cloudflare to fix the issue

I am more inclined to choose the latter option because I already use Cloudflare for other things (like their R2 storage offerings as my own image CDN - post coming soon) or their DNS. However, while waiting I will adapt my code accordingly.

Instead of using `.GitInfo` I will use the following code at the bottom of each page:

```go-html-template
{{ if .Params.gitIssue }}
<div class="post-footer">
    <small>
        {{ if .Params.lastmod }}
            This post was last updated on: 
            {{ dateFormat "January 2, 2006" .Params.lastmod}}
        <br />
        {{ else }}
            This page is under version git version control.
            <br />
        {{ end }}
            Click here: 
            <a href="https://gitlab.com/conorjwryan/conorjwryan-site/
            -/issues/{{ .Params.gitIssue }}">
            #{{ .Params.gitIssue }}
            </a>
            to view changes made.
    </small>
</div>
{{ end }}
```

This code is reliant on the custom `gitIssue` parameter being present on the post (which it always will be for the sake of versioning as stated above). I have also included the `lastmod` variable in post frontmatter to state that the page has been changed. I have to change this manually, however it is better than not having it at all.

However, as noted in the `.GitInfo` documentation, using `lastmod` with `.GitInfo` enabled through the config file (as above) is not ideal. When `.GitInfo` is enabled, regardless of it being *used*, Hugo **overwrites** any page `lastmod` value with the `.GitInfo.AuthorDate` value. Using Cloudflare Pages this will result in **ALL POSTS ACROSS THE SITE** stating they were last updated on the day of most recent repository commit...

For now I've gotten rid of the `--enableGitInfo` in my site config which fixes circumvents the `lastmod` bug and the site is at least a little bit more accountable/acuate than before. When this issue is fixed you'll know about it!

{{< img int post-footer-result.jpg "a screenshot of the current state of my post footer present on each page linking people to the git issue of the page" "the current state of my post `footer` section linking to the pages relevant git issue" >}}
