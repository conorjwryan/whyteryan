---
title: "Lessons in Hugo, WordPress and Complacency"
date: "2022-09-24"
lastmod: "2023-08-29"
description: "An Introduction to this website"
type: "post"
tags: [hugo, wordpress, personal]
gitIssue: 11
projects: [conorjwryan]
featuredImageAlt: "A screenshot of this website."
---

Over the last week I have been messing around with this website. It uses Hugo which is a static site generator, Gitlab for version control and Cloudflare Pages for hosting. This is going to be a place for me to put my projects I am working on as well as a broader development diary as I learn new skills.

{{< img int gokarna.jpg "the stock gokarna theme for hugo" "The base Gokarna theme used for this website" >}}

I am by no means a Hugo expert. I have tried it once before to minimal success but I wanted to expand my website development capabilities. Previously I had been a proponent of WordPress CMS but recently in dealing with smaller business sites I began to realise WordPress was a bit overkill. The cost of hosting and overhead costs associated made me look for a more friendly, cost-effective solution.

Don’t get me wrong, WordPress is great! It has been a staple of the web, an open-source project that is continually improving and innovating, and it provides a fertile base from which people of any skill level can make their own website for their hobbies or their business. The plugins which expands its functionality especially separate WordPress from any comparable competition. AND ITS FREE!!

{{< img int wordpress.jpg "a wordpress dashboard" "An example wordpress dashboard" >}}

My problem with WordPress is specifically one of my own making, For years I had seen WordPress as the “ONE AND ONLY” solution for web development and it wasn’t until I had started trying to start my own e-commerce business during COVID-19 (2020-2021) that I became aware of not only WordPress’ limitations but also my own. I have been creating websites for the fun of it for almost 18 years… projects based in Nginx, PHP and MySQL, like my own notes app, todo app or my own website. I knew enough to get by but did not pursue it further and as I got busier the hobby fell by the wayside.

It wasn’t until later that at my job as an IT Manager for a small family-run building supply store that I was faced with a problem with our POS (“point of service” electronic till system) where the backend reporting software was outdated and I wasn’t able to produce invoices or reports without 10 clicks and generating statements became impossible.

{{< img int builder.jpg "A cartoon of a man working on a laptop in a building supply store" "a snapshot of me working at a building supply store" >}}

I was able to create a system that the company was able to send invoices and statements and it worked great. Plus I felt like I had accomplished something, a system that I made actually being used. It had no documentation or versioning though… terrible. Still the fact remains I did something that saved my own time but also money for the company which is a plus. I wish I had kept up  that drive. Two and a half years later here we are.

A humbling experience came when I was looking for a job recently. Advice I got from a friend which I’ve now realised to be somewhat true is to get a job in IT (whether in software, security, support or web development) is you need, education, experience or portfolio. While have the experience (5 years in IT support) I do not  have the education or a portfolio… that is where this website and working on IT certifications and my realisation that I actually have a long way to build-up the other two (education and portfolio).

{{< img int gitlab.jpg "a view of my gitlab" "a view of my gitlab profile" >}}

In messing around as I have been with Hugo (and Go by extension) the last few days I have realised my “jack of all trades” hobby approach has finally hit a wall… I need to learn more and be proficient in many different areas. I guess I should’ve read the whole quote… “jack of all trades, master of none…” I know how to do a website and hosting but I am not satisfied with my limits, I want to be able to code something in JS or Rust or at least not be intimidated by it.

I am using this website as both a “portfolio” but also as a development diary as I learn these new skills. If you go to the [Projects page](/projects/) you’ll see that Hugo is one of these “projects”. While about Hugo development it is more a development diary as I learn a new skill and master this site. Hopefully doing it this way will keep me accountable too and be able to stick with something more than a few months.

{{< img int art.jpg "a cartoon of a guy looking at a bunch of code in an art gallery" "i need to show off my work as if it's an art piece" >}}

One thing that bit me in the arse a fair few times is that most of the “projects“ I’ve worked on have been private closed-source affairs. In making this website and the code surrounding it open to the public I hope this can serve as part of a portfolio while also showing my wider development process. My hope is that I will be able to point to this as something  that shows I’m not perfect, that I don’t know everything but that I can admit my faults, lack of knowledge and rectify that through self-taught experimentation, trial and error. No code is perfect but if I can show my thought process in bug remediation that would be a win.

I love creating things and while I don’t see myself as an artist or a musician I have always gone back to computers, IT and code in some form. Coding allows me to create and solve problems . I might not be the best but it’s something I want to be more than just a hobby. So here is my attempt at something like that. I’ll post updates as I encounter hurdles and overcome them. It’ll be fun. We’ll see how we go.

{{< signature standard >}}
