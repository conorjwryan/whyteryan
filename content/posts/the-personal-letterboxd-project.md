---
title: "The Personal Letterboxd Project"
date: 2022-09-30T17:30:45+01:00
draft: false
lastmod: "2023-08-29"
description: "An Introduction to the 'Movie Madness' Project"
type: "post"
tags: [personal, letterboxd]
gitIssue: 18
projects: [moviemadness]
featuredImageAlt: "A screenshot of my letterboxd account"
featuredImageCaption: "a screenshot of my letterboxd account"
---

## The Overview

Letterboxd is an amazing, movie-focused social network. It allows people to log the films they've watched and rated while also giving people the chance to interact with friends and get recommendations. [I love it and use it daily](https://letterboxd.com/conorjwryan/). Currently, as of writing this, I have seen 256 films this year alone. It is almost perfect. Almost...

## The Problem

I was trying to recommend a film to a friend the other day and couldn't find it, even knowing what I was looking for. I came to realise two things.

1. I can't search by simple genre. It is almost impossible to search something I've seen.
2. The whole profile experience is too overwhelming for people whether they use the service but especially if they don't. There is too much to click.

{{< img int organisation-options.jpg "there are some options available but they are not obvious" "there are some options available but they are not obvious" >}}

## The Solution

Now there are film organisation options. These come in the form of either tags or lists. You can also sort by highest/lowest rated. But there is no way to easily display the films by genre unless you tag every film and put them into personal lists.

{{< img int tags.jpg "the tags section of my profile" "the tags section of my profile." >}}

I could just tag each individual film and review with about 5 or 6 tags but I only see that as part of the solution to this problem. The way I it there is a fundamental need to redesign the profile section to make it more friendly. Don't get me wrong I like all the granular controls but it needs to be presented in a different way. A way that works better for people regardless of if they know how to navigate letterboxd or not.

I intend to create my own site based on the information on my letterboxd profile. My goal is to make it easy for someone to find a film and easy to decide if they should check it out.

## The Process

As I see it there are two ways to go about this. Since I am using data from Letterboxd it would make sense to use their API in order to get the information. This would provide all the information I need and my job would be to organise and present it. Life is never that simple.

{{< img int api-says-no.jpg "The API is currently in private better" "The API is not freely available... there is another way" >}}

I could apply but without a proven concept I don't think it's likely that I will be accepted. There is also no guarantee when the API will be open to the public. Rather see the API as the second step to this project, when I can prove to the people at Letterboxd that this isn't just a concept or an idea but something with a real working prototype. So as it says above I will use the means at my disposal to complete this project.

## The Challenges

{{< img int export.jpg "a popup notice saying I can export my film data in a .CSV file" "I can export my data in a .csv file!" >}}

Getting the data for this project in and of itself is not an issue. Keeping that issue up-to-date is. Letterboxd provides two different means to get my film data and keep it up to date. Exporting my data as a .csv and keeping that data up-to-date by using RSS.

## In Closing

I really like Letterboxd, I really like movies. I used to want to be a film reviewer and have been writing reviews on and off since 2017 (when I registered the domain moviemadness.uk!). Doing this project allows me to combine my love for movies with my hobby of coding.

I'll post more in this project as I progress. Keep an eye here (conorjwryan.com) for updates but also on the movie-madness [gitlab](https://gitlab.com/conorjwryan/movie-madness) for the code.

{{< signature standard >}}

{{< img int movie-stats.jpg "click to view my letterboxd stats page 280 films this year means 494 hours well spent" "click to view my letterboxd stats page" "https://letterboxd.com/conorjwryan/year/2022/" >}}
