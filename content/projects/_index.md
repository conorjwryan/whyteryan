---
title: "Personal Projects"
---

This is a list of my personal projects. They range from websites to script repositories, and are all hosted on GitLab. I have included a brief description of each project below. Click through for more information and for a list of articles I have written within project.
