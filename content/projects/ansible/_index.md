---
title: "Ansible Scripts"
linktitle: "ansible"
type: project
lastmod: "2024-08-01"
summary: Ansible is an automation tool that allows you to manage your infrastructure through code. I use Ansible to manage my personal and client infrastructure. This project contains my Ansible playbooks and roles.
shortDescription: "A repository of my Ansible scripts helping me automate and manage my infrastructure"
linkGit: "https://gitlab.com/conorjwryan/ansible"
gitIssue: 54
reason: "Professional"
started: "2023-07-13"
weight: 4
featuredImage: "https://cdn.cjri.uk/conorjwryan/static/images/project-ansible.jpg"
---

Ansible is an automation tool that allows you to manage your infrastructure through code. It is a great tool for managing multiple servers and services. I use Ansible to manage my personal and client infrastructure.

My scripts revolve around updating and maintaining my code repositories across multiple servers, as well as updating various services and packages. I also use Ansible to manage my Docker containers and their associated volumes.
