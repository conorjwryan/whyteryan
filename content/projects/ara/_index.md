---
title: "Project Ara"
linktitle: "ara"
aka: "Ara Dockerised WordPress"
type: "project"
lastmod: "2024-08-01"
summary : Ara is a docker-based WordPress deployment system using Docker Compose allowing for easy deployment and management of WordPress sites. Can be used for development or production.
shortDescription: "Easy WordPress deployment using Docker Compose with the official WordPress and MariaDB images."
linkGit: "https://gitlab.com/conorjwryan/ara"
gitIssue: 52
reason: "Professional"
started: "2023-07-06"
weight: 2
featuredImage: "https://cdn.cjri.uk/conorjwryan/static/images/project-ara-wordpress-logo.jpg"
---

Ara is a dockerised WordPress compose repository based on the official docker WordPress and MariaDB images. Managing many WordPress sites across a load of private repositories became time consuming as I was constantly applying the same fixes. Project Ara is my attempt to make the deployment and maintenance of my WordPress installations seamless by containerising them with Docker.

You can run multiple independent instances of Ara on the same system through a reverse proxy like Cloudflared or NPM. The reverse proxy allows for finer control of SSL, TLS and other secure connection related settings without the need to further fine-tune specific server settings.
