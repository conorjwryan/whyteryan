---
title: "Bash Scripts"
linktitle: "bash"
type: project
lastmod: "2024-08-01"
summary: "A collection of bash scripts for system administration. Currently focused on backup and API interaction. Each script has detailed instructions on how to set up and use. "
shortDescription: "A collection of bash scripts for system administration. Focused on backup and API interaction."
linkGit: "https://gitlab.com/conorjwryan/scripts"
gitIssue: 50
reason: "Professional"
started: "2023-03-01"
weight: 3
featuredImage: "https://cdn.cjri.uk/conorjwryan/static/images/project-bash-new.jpg"
---

I have created a repository for the bash scripts I use on a regular basis. My goal is to create scripts which are both platform and system agnostic so as to run on any system, based on a selection of defined environmental variables. The scripts are designed to be used in a modular fashion, with each script being able to be used independently of the others. The idea being that they can be run at a scheduled time through cronjobs, or manually as required.

Currently the scripts focus on backup of key folders and API interaction. The intention is to expand the scope to include other areas of system administration in the future. These scripts are designed to be able to be used on any system with a bash shell. Right now they have only been tested on Linux and MacOS systems.
