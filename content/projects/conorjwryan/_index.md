---
title: "Personal Website"
linktitle: "conorjwryan"
type: project
lastmod: "2024-08-01"
summary: "This website is built using the Hugo static site generator and hosted on Cloudflare Pages with GitLab integration. It features tutorials on Hugo and Cloudflare, with its source code freely available on GitLab."
shortDescription: "A place to host my thoughts, projects and tutorials on Cloudflare and Hugo."
linkGit: "https://gitlab.com/conorjwryan/conorjwryan-site"
gitIssue: 24
linkUrl: "https://www.conorjwryan.com"
started: "2022-09-16"
reason: "Professional"
aka: "conorjwryan.com"
weight: 1
featuredImage: "https://cdn.cjri.uk/conorjwryan/static/images/conorjwryan-new-2.jpg"
--- 

This is the project for this website. It is based on the Hugo static site generator, hosted on Cloudflare Pages, with help from GitLab. I started this project because it is good practice to have a base to put all my thoughts and list my publicly available projects.

Posts in this project range from tutorials on how to use Cloudflare and Hugo, to my thoughts on the state of the internet and the world. My hope is it will be useful to those exploring Hugo, self-hosting, WordPress or Cloudflare. This website source is freely available on GitLab.
