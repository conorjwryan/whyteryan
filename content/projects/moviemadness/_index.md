---
linktitle: "moviemadness"
title: "Movie Madness"
lastmod: "2024-08-01"
type: project
summary: "Letterboxd is a movie-focused social media site. Movie Madness is a project to create a site that displays my movie reviews from letterboxd in a more inviting way."
shortDescription: "A project to create a site that displays my movie reviews from letterboxd."
linkUrl: "https://www.moviemadness.uk"
linkGit: "https://gitlab.com/conorjwryan/movie-madness"
gitIssue: 22
reason: "Hobby"
started: "2022-09-30"
weight: 5
featuredImage: "https://cdn.cjri.uk/conorjwryan/static/images/the-personal-letterboxd-project.jpg"
---
Letterboxd is a movie-focused social network I use to log and review the movies that I’ve watched throughout the year. I love it, and use it daily. I've always liked the idea of having my own movie site where I can host my own reviews. Letterboxd is great for that but the profile page could be better organised to easily find recommendations.

Enter the 'movie madness' project where I attempt to translate the data from my letterboxd to my own movie website to solve this (admittedly very niche) problem. I do not see this project as a replacement for the letterboxd profile but rather an extension of its functionality. My hope is that when this project is done, other people can use this open-source project to show off their own profiles.
