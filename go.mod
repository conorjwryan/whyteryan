module gitlab.com/conorjwryan/conorjwryan-site

go 1.21.0

require (
	github.com/adityatelange/hugo-PaperMod v0.0.0-20230812122200-9d4a9e825a10 // indirect
	gitlab.com/Roneo/hugo-shortcode-roneo-collection v0.0.0-20230820055121-2836bb0f899d // indirect
)
